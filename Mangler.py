from cnorm.nodes import (PrimaryType, Decl, FuncType)
from pyrser import (meta)


class Mangler(object):
    primary_types = {
        'char': 'c',
        'int': 'i',
        'float': 'f',
        'double': 'd',
        'short':'s',
        'bool':'b',
        'long':'l',
        'long long':'x',
        'signed char':'Sc',
        'void': 'v'
    }
    sign = {
        'signed': '',
        'unsigned': 'U',
        'auto': 'A'
    }

    def type_mnemonic(self, t, s='signed'):
        if t not in self.primary_types:
            raise IndexError("Unknown mangling type '{t}'".format(t=t))
        mangle = self.sign.get(s, 'BugMangleSign') + \
                 self.primary_types.get(t, 'BugManglePrimaryType')
        return mangle

@meta.add_method(Decl)
def manglify(self):
    if hasattr(self._ctype, 'manglify'):
        mangled_params = ''
        if isinstance(self._ctype, FuncType):
            mangled_params += ''

@meta.add_method(Decl)
def manglify_ctype(self):
    if hasattr(self._ctype, 'manglify'):
        return self._ctype.manglify()

@meta.add_method(FuncType)
def manglify_params(self):
    if len(self.params) == 0:
        mangler = Mangler()
        return mangler.type_mnemonic("void")
    mangled_params = (param.manglify_ctype if hasattr(param, 'manglify_ctype')
                                           else '' for param in self.params)
    return ''.join(mangled_params)

@meta.add_method(PrimaryType)
def manglify(self):
    mangler = Mangler()
    decl_type = ''
    if self._decltype is not None and hasattr(self._decltype, "manglify"):
        decl_type = self._decltype.manglify()
    return decl_type + mangler.type_mnemonic(self._identifier)
