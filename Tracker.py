from inspect import (stack)


def default_fortracker(*args):
    pass


def default_backtracker(*args):
    pass


class Tracker(object):

    def __init__(self, fortrack=default_fortracker,
                 backtrack=default_backtracker, *args, **kwargs):
        self.fortrack = fortrack
        self.backtrack = backtrack
        self.args = args
        self.kwargs = kwargs

    def _getFuncName(self, shift=0):
        return stack()[4+shift][3]

    def __call__(self, f):
        def _f(*args, **kwargs):
            self.fortrack(*args)
            r = f(*args)
            self.backtrack(*args)
            return r
        return _f


class ParentRegister(Tracker):
    def __init__(self, *args, **kwargs):
        super(ParentRegister, self).__init__(self.fortrack, *args, **kwargs)

    def fortrack(self, motherofall, node, parent, *args):
        if type(node).__name__ not in ["list", "str"]:
            node.ADBG_parent = lambda: parent
        return


class NodeIdRegister(Tracker):
    def __init__(self, *args, **kwargs):
        super(NodeIdRegister, self).__init__(self.fortrack, *args, **kwargs)

    def fortrack(self, motherofall, node, parent, *args):
        if type(node).__name__ not in ["list", "str"]:
            if not hasattr(motherofall, "nodeid"):
                motherofall.nodeid = 0
            node.ADBG_node_id = motherofall.nodeid
            motherofall.nodeid += 1
        return


class ScopeRegister(Tracker):
    def __init__(self, *args, **kwargs):
        super(ScopeRegister, self).__init__(self.fortrack, self.backtrack,
                                            *args, **kwargs)
        self.symId = 0
        self.scopes = [
            {
                "name": None,
                "type": "global",
                "symbols": {},
            },
        ]
        # symbols
        #   name:
        #       id: unique identifier
        #       ctype: symbol's type

    def currentScope(self):
        return self.scopes[len(self.scopes) - 1]

    def newScope(self, name, _type, symbols):
        self.scopes.append({
            "name": name,
            "type": _type,
            "symbols": symbols,
        })

    def registerDecl(self, decl):
        scope = self.currentScope()["symbols"]
        if decl._name in scope.keys():
            print("ERROR: symbol already exists in this scope")
            return
        scope[decl._name] = {
            "id": self.symId,
            "ctype": decl._ctype
        }
        self.symId += 1

    def fortrack(self, motherofall, node, parent, *args):
        if type(node).__name__ in ["list", "str"]:
            return
        if type(node).__name__ == "RootBlockStmt":
            node.ADBG_scope = self.scopes[len(self.scopes) - 1]
        if type(node).__name__ == "Decl":
            if type(parent).__name__ not in ["RootBlockStmt", "BlockStmt"]:
                return
            self.registerDecl(node)
            # new symbol
        if type(node).__name__ == "BlockStmt"\
                and type(parent).__name__ == "Decl":
            self.newScope(parent._name, "local", {})
            node.ADBG_scope = self.currentScope()
            for param in parent._ctype._params:
                self.registerDecl(param)
        # when entering func:
        #   [x] register function in current scope
        #   [x] create new scope
        #   [x] save scope on func node
        #   [x] register arguments in new scope
        # when new symbols:
        #   [x] register in scope
        return

    def backtrack(self, motherofall, node, parent, *args):
        if type(node).__name__ == "BlockStmt"\
                and type(parent).__name__ == "Decl":
            self.scopes.pop()
        # when leaving func:
        #   [x] leave scope
        return
