class Thesis(object):
    def __init__(self, output=None):
        self._output = output

    def print(self, text, indent=0):
        if isinstance(text, list):
            for element in text:
                self.print(str(" "*indent)+"* "+text)
        else:
            if self._output is not None:
                self._output.write(text)
            else:
                print(text)

    def quote(self, text, level=1):
        self.print("{level_quote} {text}\n".format(level_quote=level*">",
                                                   text=text))

    def title(self, text, level=1):
        if level > 6:
            level = 6
        elif level <= 0:
            level = 1
        title = "{title_level} {text}\n".format(title_level="#"*level,
                                                text=text)
        self.print(title)

    def code(self, code, language="c"):
        text = "```{language}\n{code}\n```\n".format(language=language,
                                                   code=code)
        self.print(text)

    def inline_code(self, code):
        self.print("`{code}`".format(code=code))

    def emphasis(self, text):
        self.print("_{text}_".format(text=text))

    def strong(self, text):
        self.print("__{text}__".format(text=text))
