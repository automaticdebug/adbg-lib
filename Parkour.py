from cnorm.parsing.declaration import (Declaration)
from . import (Logger)


class Parkour(object):

    def __init__(self, filename, verbosity):
        super(Parkour, self).__init__()
        self._file = filename
        self._cparser = Declaration()
        self._ast = self._cparser.parse_file(filename)
        self._logger = Logger.getLogger(verbosity, self._file)

    def _func(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _blockInit(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _blockExpr(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _unary(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _paren(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _array(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _dot(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _arrow(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _post(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _sizeof(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _binary(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _cast(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _range(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _ternary(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _terminal(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _id(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _literal(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _raw(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _enumerator(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _declType(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _pointerType(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _arrayType(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _parenType(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _qualType(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _attrType(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _cType(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _primaryType(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _composedType(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _funcType(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _decl(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _stmt(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _exprStmt(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _blockStmt(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _rootBlockStmt(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _label(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _branch(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _case(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _return(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _goto(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _loopControl(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _continue(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _break(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _conditional(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _if(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _while(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _switch(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _do(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _for(self, node, parent, *args, **kwargs):
        self._logger.debug(node)

    def _p_func(self, node, parent):
        self._func(node, parent)
        self.switch_attr(node, parent)

    def _p_blockInit(self, node, parent):
        self._blockInit(node, parent)
        self.switch_attr(node, parent)

    def _p_blockExpr(self, node, parent):
        self._blockExpr(node, parent)
        self.switch_attr(node, parent)

    def _p_unary(self, node, parent):
        self._unary(node, parent)
        self.switch_attr(node, parent)

    def _p_paren(self, node, parent):
        self._paren(node, parent)
        self.switch_attr(node, parent)

    def _p_array(self, node, parent):
        self._array(node, parent)
        self.switch_attr(node, parent)

    def _p_dot(self, node, parent):
        self._dot(node, parent)
        self.switch_attr(node, parent)

    def _p_arrow(self, node, parent):
        self._arrow(node, parent)
        self.switch_attr(node, parent)

    def _p_post(self, node, parent):
        self._post(node, parent)
        self.switch_attr(node, parent)

    def _p_sizeof(self, node, parent):
        self._sizeof(node, parent)
        self.switch_attr(node, parent)

    def _p_binary(self, node, parent):
        self._binary(node, parent)
        self.switch_attr(node, parent)

    def _p_cast(self, node, parent):
        self._cast(node, parent)
        self.switch_attr(node, parent)

    def _p_range(self, node, parent):
        self._range(node, parent)
        self.switch_attr(node, parent)

    def _p_ternary(self, node, parent):
        self._ternary(node, parent)
        self.switch_attr(node, parent)

    def _p_terminal(self, node, parent):
        self._terminal(node, parent)

    def _p_id(self, node, parent):
        self._id(node, parent)
        self.switch_attr(node, parent)

    def _p_literal(self, node, parent):
        self._literal(node, parent)
        self.switch_attr(node, parent)

    def _p_raw(self, node, parent):
        self._raw(node, parent)
        self.switch_attr(node, parent)

    def _p_enumerator(self, node, parent):
        self._enumerator(node, parent)

    def _p_declType(self, node, parent):
        self._declType(node, parent)

    def _p_pointerType(self, node, parent):
        self._pointerType(node, parent)

    def _p_arrayType(self, node, parent):
        self._arrayType(node, parent)
        self.switch_attr(node, parent)

    def _p_parenType(self, node, parent):
        self._parenType(node, parent)
        self.switch_attr(node, parent)

    def _p_qualType(self, node, parent):
        self._qualType(node, parent)
        self.switch_attr(node, parent)

    def _p_attrType(self, node, parent):
        self._attrType(node, parent)
        self.switch_attr(node, parent)

    def _p_cType(self, node, parent):
        self._cType(node, parent)
        self.switch_attr(node, parent)

    def _p_primaryType(self, node, parent):
        self._primaryType(node, parent)
        self.switch_attr(node, parent)

    def _p_composedType(self, node, parent):
        self._composedType(node, parent)
        self.switch_attr(node, parent)

    def _p_funcType(self, node, parent):
        self._funcType(node, parent)
        self.switch_attr(node, parent)

    def _p_decl(self, node, parent):
        self._decl(node, parent)
        self.switch_attr(node, parent)

    def _p_stmt(self, node, parent):
        self._stmt(node, parent)

    def _p_exprStmt(self, node, parent):
        self._exprStmt(node, parent)
        self.switch_attr(node, parent)

    def _p_blockStmt(self, node, parent):
        self._blockStmt(node, parent)
        self.switch_attr(node, parent)

    def _p_rootBlockStmt(self, node, parent):
        self._rootBlockStmt(node, parent)
        self.switch_attr(node, parent)

    def _p_label(self, node, parent):
        self._label(node, parent)

    def _p_branch(self, node, parent):
        self._branch(node, parent)
        self.switch_attr(node, parent)

    def _p_case(self, node, parent):
        self._case(node, parent)
        self.switch_attr(node, parent)

    def _p_return(self, node, parent):
        self._return(node, parent)
        self.switch_attr(node, parent)

    def _p_goto(self, node, parent):
        self._goto(node, parent)
        self.switch_attr(node, parent)

    def _p_loopControl(self, node, parent):
        self._loopControl(node, parent)

    def _p_continue(self, node, parent):
        self._continue(node, parent)

    def _p_break(self, node, parent):
        self._break(node, parent)

    def _p_conditional(self, node, parent):
        self._conditional(node, parent)
        self.switch_attr(node, parent)

    def _p_if(self, node, parent):
        self._if(node, parent)
        self.switch_attr(node, parent)

    def _p_while(self, node, parent):
        self._while(node, parent)
        self.switch_attr(node, parent)

    def _p_switch(self, node, parent):
        self._switch(node, parent)
        self.switch_attr(node, parent)

    def _p_do(self, node, parent):
        self._do(node, parent)
        self.switch_attr(node, parent)

    def _p_for(self, node, parent):
        self._for(node, parent)
        self.switch_attr(node, parent)

    def _p_chainmap(self, node, parent):
        pass

    def _p_list(self, _node, parent):
        for node in _node:
            self.parkour(node, parent)

    def _p_str(self, node, parent):
        pass

    def _p_node(self, node, parent):
        pass

    def _p_default(self, node, parent):
        self._logger.error("{} -> {}".format(type(node).__name__,
                                             node))

    def parkour(self, node, parent=None):
        return self.switch_func(node, parent)

    def switch_func(self, node, parent):
        switcher = {
            'Func': self._p_func,
            'BlockInit': self._p_blockInit,
            'BlockExpr': self._p_blockExpr,
            'Unary': self._p_unary,
            'Paren': self._p_paren,
            'Array': self._p_array,
            'Dot': self._p_dot,
            'Arrow': self._p_arrow,
            'Post': self._p_post,
            'Sizeof': self._p_sizeof,
            'Binary': self._p_binary,
            'Cast': self._p_cast,
            'Range': self._p_range,
            'Ternary': self._p_ternary,
            'Terminal': self._p_terminal,
            'Id': self._p_id,
            'Literal': self._p_literal,
            'Raw': self._p_raw,
            'Enumerator': self._p_enumerator,
            'DeclType': self._p_declType,
            'PointerType': self._p_pointerType,
            'ArrayType': self._p_arrayType,
            'ParenType': self._p_parenType,
            'QualType': self._p_qualType,
            'AttrType': self._p_attrType,
            'CType': self._p_cType,
            'PrimaryType': self._p_primaryType,
            'ComposedType': self._p_composedType,
            'FuncType': self._p_funcType,
            'Decl': self._p_decl,
            'ExprStmt': self._p_exprStmt,
            'BlockStmt': self._p_blockStmt,
            'RootBlockStmt': self._p_rootBlockStmt,
            'Label': self._p_label,
            'Branch': self._p_branch,
            'Case': self._p_case,
            'Return': self._p_return,
            'Goto': self._p_goto,
            'LoopControl': self._p_loopControl,
            'Continue': self._p_continue,
            'Break': self._p_break,
            'Conditional': self._p_conditional,
            'If': self._p_if,
            'While': self._p_while,
            'Switch': self._p_switch,
            'Do': self._p_do,
            'For': self._p_for,
            'ChainMap': self._p_chainmap,
            'list': self._p_list,
            'str': self._p_str,
            'Node': self._p_node
        }
        f = switcher.get(type(node).__name__,
                         lambda node, parent: self._p_default(node, parent))
        return f(node, parent)

    def _params(self, params, parent):
        self._logger.debug("PARAMS:" + str(params))

    def _body(self, body, node):
        self._logger.debug("BODY:" + str(body))

    def _diagnostic(self, diagnostic, parent):
        self._logger.debug("DIAGNOSTIC:" + str(diagnostic))

    def _value(self, value, parent):
        self._logger.debug("VALUE:" + str(value))

    def __ctype(self, _ctype, parent):
        self._logger.debug("_CTYPE:" + str(_ctype))

    def _types(self, types, parent):
        self._logger.debug("TYPES:" + str(types))

    def _expr(self, expr, parent):
        self._logger.debug("EXPR:" + str(expr))

    def __name(self, _name, parent):
        self._logger.debug("_NAME:" + str(_name))

    def _thencond(self, thencond, parent):
        self._logger.debug("THENCOND:" + str(thencond))

    def _elsecond(self, elsecond, parent):
        self._logger.debug("ELSECOND:" + str(elsecond))

    def _call_expr(self, call_expr, parent):
        self._logger.debug("CALL_EXPR:" + str(call_expr))

    def __colon_expr(self, _colon_expr, parent):
        self._logger.debug("_COLON_EXPR:" + str(_colon_expr))

    def _condition(self, condition, parent):
        self._logger.debug("CONDITION:" + str(condition))

    def _increment(self, increment, parent):
        self._logger.debug("INCREMENT:" + str(increment))

    def _init(self, init, parent):
        self._logger.debug("INIT:" + str(init))

    def __identifier(self, _identifier, parent):
        self._logger.debug("_IDENTIFIER:" + str(_identifier))

    def __storage(self, _storage, parent):
        self._logger.debug("_STORAGE:" + str(_storage))

    def __assign_expr(self, _assign_expr, parent):
        self._logger.debug("_ASSIGN_EXPR:" + str(_assign_expr))

    def __decltype(self, _decltype, parent):
        self._logger.debug("_DECLTYPE:" + str(_decltype))

    def __specifier(self, _specifier, parent):
        self._logger.debug("_SPECIFIER:" + str(_specifier))

    def __params(self, _params, parent):
        self._logger.debug("_PARAMS:" + str(_params))

    def _fields(self, fields, parent):
        self._logger.debug("FIELDS:" + str(fields))

    def __sign(self, _sign, parent):
        self._logger.debug("_SIGN:" + str(_sign))

    def _p_params(self, node, parent):
        self._params(node, parent)
        self.parkour(node, parent)

    def _p_body(self, node, parent):
        self._body(node, parent)
        self.parkour(node, parent)

    def _p_diagnostic(self, node, parent):
        self._diagnostic(node, parent)

    def _p_value(self, node, parent):
        self._value(node, parent)

    def _p__ctype(self, node, parent):
        self.__ctype(node, parent)
        self.parkour(node, parent)

    def _p_types(self, node, parent):
        self._types(node, parent)
        self.parkour(node, parent)

    def _p_expr(self, node, parent):
        self._expr(node, parent)
        self.parkour(node, parent)

    def _p__name(self, node, parent):
        self.__name(node, parent)
        self.parkour(node, parent)

    def _p_thencond(self, node, parent):
        self._thencond(node, parent)
        self.parkour(node, parent)

    def _p_elsecond(self, node, parent):
        self._elsecond(node, parent)
        self.parkour(node, parent)

    def _p_call_expr(self, node, parent):
        self._call_expr(node, parent)
        self.parkour(node, parent)

    def _p__colon_expr(self, node, parent):
        self.__colon_expr(node, parent)
        self.parkour(node, parent)

    def _p_condition(self, node, parent):
        self._condition(node, parent)
        self.parkour(node, parent)

    def _p_increment(self, node, parent):
        self._increment(node, parent)
        self.parkour(node, parent)

    def _p_init(self, node, parent):
        self._init(node, parent)
        self.parkour(node, parent)

    def _p__identifier(self, node, parent):
        self.__identifier(node, parent)

    def _p__storage(self, node, parent):
        self.__storage(node, parent)

    def _p__assign_expr(self, node, parent):
        self.__assign_expr(node, parent)
        self.parkour(node, parent)

    def _p__decltype(self, node, parent):
        self.__decltype(node, parent)
        self.parkour(node, parent)

    def _p__specifier(self, node, parent):
        self.__specifier(node, parent)

    def _p__params(self, node, parent):
        self.__params(node, parent)
        self.parkour(node, parent)

    def _p_fields(self, node, parent):
        self._fields(node, parent)
        self.parkour(node, parent)

    def _p__sign(self, node, parent):
        self.__sign(node, parent)
        self.parkour(node, parent)

    # Type defined by Yaz, pass it
    def _p_ADBG_parent(self, node, parent):
        pass

    def _p_lineno(self, node, parent):
        pass

    # Type defined by Yaz, pass it
    def _p_ADBG_node_id(self, node, parent):
        pass

    # Type defined by Yaz, pass it
    def _p_ADBG_scope(self, node, parent):
        pass

    def _p_default_attr(self, node, key):
        self._logger.error("Ooops")
        self._logger.error("\""+str(key)+"\""
                           + " is not set in switch_attr tab.")
        self._logger.error("\""+str(key)+"\""
                           + " attr found in the following node :")
        self._logger.error(node)

    def switch_attr(self, node, parent):
        switcher = {
            'params': self._p_params,
            'condition': self._p_condition,
            'body': self._p_body,
            'expr': self._p_expr,
            '_expr': self._p_expr,
            'value': self._p_value,
            'types': self._p_types,
            'diagnostic': self._p_diagnostic,
            '_name': self._p__name,
            'call_expr': self._p_call_expr,
            '_colon_expr': self._p__colon_expr,
            'thencond': self._p_thencond,
            'elsecond': self._p_elsecond,
            '_assign_expr': self._p__assign_expr,
            'increment': self._p_increment,
            'init': self._p_init,
            '_identifier': self._p__identifier,
            '_storage': self._p__storage,
            '_decltype': self._p__decltype,
            '_specifier': self._p__specifier,
            '_params': self._p__params,
            'fields': self._p_fields,
            '_sign': self._p__sign,
            'ADBG_parent': self._p_ADBG_parent,  # ADBG_Type
            'ADBG_node_id': self._p_ADBG_node_id,  # ADBG_Type
            'ADBG_scope': self._p_ADBG_scope,  # ADBG_Type
            '_ctype': self._p__ctype,
            'lineno': self._p_lineno,
        }
        for key in node.__dict__.keys():
            if type(node.__dict__[key]).__name__ in ["str", "int", "NoneType"]:
                continue
            f = switcher.get(key, lambda node,
                             parent: self._p_default_attr(node, key))
            f(node.__dict__[key], node)

    def getChildren(self, node):
        excludeTypes = [
            "str", "int", "NoneType", "function", "dict"
        ]
        children = []
        for key in node.__dict__.keys():
            kid = node.__dict__[key]
            if type(kid).__name__ in excludeTypes:
                continue
            if type(kid).__name__ == "list":
                for k in kid:
                    if type(k).__name__ not in excludeTypes:
                        children.append(k)
            else:
                children.append(kid)
        return children

    def getParent(self, node, depth):
        if depth == 0:
            return node
        if node.ADBG_parent() is None:
            return node
        return self.getParent(node.ADBG_parent(), depth - 1)

    def getScope(self, node, name=None):
        if name is None and type(node).__name__ != "Id":
            self._logger.error("getScope: node must be an Id")
            return None
        if name is None:
            name = node.value
        if type(node).__name__ not in ["RootBlockStmt", "BlockStmt"]:
            return self.getScope(node.ADBG_parent(), name)
        if type(node).__name__ == "BlockStmt"\
                and type(node.ADBG_parent()).__name__ != "Decl":
            return self.getScope(node.ADBG_parent(), name)
        if name in node.ADBG_scope["symbols"].keys():
            return {
                "scope": node.ADBG_scope,
                "symbol": node.ADBG_scope["symbols"][name]
            }
        if type(node).__name__ == "RootBlockStmt":
            return None
        return self.getScope(node.ADBG_parent(), name)

    def run(self):
        self._logger.info("parkour launched")
        self.parkour(self._ast)
        self._logger.info("parkour ended")

    def __str__(self):
        return "<Parkour file={file}>".format(file=self._file)

    def __unicode__(self):
        return u"{}".format(self.__str__())
