import logging

def getLogger(verbosity, name):
    logger = logging.getLogger(name)
    logger.setLevel(verbosity)
    ch = logging.StreamHandler()
    ch.setLevel(verbosity)
    formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger
