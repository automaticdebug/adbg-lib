from argparse import (Action)
import logging

class LoggingAction(Action):


    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None:
            raise ValueError("nargs are not allowed")
        super(LoggingAction, self).__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        if values.upper() == "DEBUG":
            setattr(namespace, self.dest, logging.DEBUG)
        elif values.upper() == "INFO":
            setattr(namespace, self.dest, logging.INFO)
        elif values.upper() == "WARNING":
            setattr(namespace, self.dest, logging.WARNING)
        elif values.upper() == "ERROR":
            setattr(namespace, self.dest, logging.ERROR)
        elif values.upper() == "CRITICAL":
            setattr(namespace, self.dest, logging.CRITICAL)
        else:
            setattr(namespace, self.dest, logging.NOTSET)

